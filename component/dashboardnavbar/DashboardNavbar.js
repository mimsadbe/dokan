import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledButtonDropdown,
} from "reactstrap";
const DashboardNavbar = () => {
  return (
    <div className="container">
      <nav className="navbar">
        <UncontrolledButtonDropdown>
          <DropdownToggle caret className="dropdown" color="">
            توسعه آلماتک
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>Header</DropdownItem>
            <DropdownItem disabled>Action</DropdownItem>
            <DropdownItem>Another Action</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Another Action</DropdownItem>
          </DropdownMenu>
        </UncontrolledButtonDropdown>
      </nav>
    </div>
  );
};

export default DashboardNavbar;
