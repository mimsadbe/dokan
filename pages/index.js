import Head from "next/head";
import Link from "next/link";

export default function Home() {
  return (
    <div className="bg_home">
      <div className={`container`}>
        <Head>
          <title>Welcome To Dashboard</title>
          <meta name="description" content="hojre dashboard panel" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <header className="header_home">
          <div className="navbar_home">
            <Link href="/login">LOGIN</Link>
            <span>PACKAGE</span>
            <span>CONTACT</span>
            <span>HOME</span>
          </div>
          <div className="logo_home">
            <img src="/img/almalogo.png" alt="hojre" />
          </div>
        </header>
        <main>
          <div className="image_home"></div>
          <div className="content_home">
            <div>

            </div>
          </div>
        </main>
      </div>
    </div>
  );
}
