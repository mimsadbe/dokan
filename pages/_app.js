import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import { useRouter } from "next/router";
import Head from "next/head";
import { useState } from "react";
import DashboardNavbar from "/component/dashboardnavbar/DashboardNavbar";
import AsideDashboard from "/component/asideDashboard/AsideDashboard";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  if (router.asPath.split("/")[1] == "dashboard") {
    return (
      <div className="d-flex">
        <Head>
          <title>HOJRE DASHBOARD</title>
          <meta name="description" content="dashbord" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <AsideDashboard
          data={[
            {
              title: "ثبت محتوا",
              child: [
                { childTitle: "ثبت نویسنده", href: "/test1" },
                { childTitle: "ثبت بلاگ", href: "/test2" },
                { childTitle: "ثبت ناشر", href: "/test3" },
              ],
            },
            {
              title: "لیست محتوا",
              child: [
                { childTitle: "لیست نویسنده", href: "/test5" },
                { childTitle: "لیست بلاگ", href: "/test6" },
                { childTitle: "لیست ناشر", href: "/test7" },
              ],
            },
          ]}
        />
        <main className="main">
          <DashboardNavbar />
          <section className="container">
            <form
              className="w-100"
              action="https://formspree.io/f/mdoybkzq"
              method="POST"
            >
              <Component {...pageProps} />
            </form>
          </section>
        </main>
      </div>
    );
  } else {
    return <Component {...pageProps} />;
  }
}

export default MyApp;
