import Input from "/component/form/input/Input";
import Checkbox from "/component/form/checkbox/Checkbox";
import Link from 'next/link';
const login = () => {
  return (
    <div className="login_page">
      <div className="login_box">
        <Input
          data={[
            {
              title_label: "نام کاربری",
              name: "username",
              info: "* نام کاربری ایمیل شماست",
              validator: ["isEmpty", "isEmail"],
              invalid_message: ["خالی نذار !!", "این چه طرز وارد کردن ایمیله !!"],
            },
          ]}
        />
        <Input
          data={[
            {
              title_label: "پسورد",
              name: "passwprd",
              type: "password",
              validator: ["isEmpty"],
              invalid_message: ["پسوردش کو ؟؟؟"],
            },
          ]}
        />
        <Checkbox title_label="مرا به خاطر بسپار" />
          <Link href="/dashboard">
            <a className="btnsubmit">
              ورود
            </a>
          </Link>
      </div>
    </div>
  );
};

export default login;
